import { Box, Button, IconButton, styled, Typography } from "@material-ui/core";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import React, { useState } from "react";

const DigitContainer = styled(Box)({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
});

const PadDigitsContainer = styled(Box)({
  display: "flex",
  justifyContent: "center",
});

const Digit = ({ digit, index, updateDigit }) => {
  return (
    <DigitContainer>
      <IconButton onClick={() => updateDigit(index, -1)}>
        <ExpandLess />
      </IconButton>
      <Typography variant="h6">{digit}</Typography>
      <IconButton onClick={() => updateDigit(index, +1)}>
        <ExpandMore />
      </IconButton>
    </DigitContainer>
  );
};

const UnlockPad = ({ unlockCode, unlock }) => {
  const initialValue = [0, 0, 0, 0];
  const [code, setCode] = useState(initialValue);

  const updateDigit = (index, delta) => {
    const newCode = [...code];
    newCode[index] =
      newCode[index] + delta < 0
        ? 9
        : newCode[index] + delta > 9
        ? 0
        : newCode[index] + delta;
    setCode(newCode);
  };

  const tryCode = () => {
    if (unlockCode === code.join("")) {
      unlock();
    } else {
      setCode(initialValue);
    }
  };

  return (
    <Box>
      <PadDigitsContainer>
        {code.map((digit, index) => (
          <Digit
            key={`digit-${index}`}
            digit={digit}
            index={index}
            updateDigit={updateDigit}
          />
        ))}
      </PadDigitsContainer>
      <Button fullWidth variant="contained" color="primary" onClick={tryCode}>
        Probar
      </Button>
    </Box>
  );
};

export default UnlockPad;
