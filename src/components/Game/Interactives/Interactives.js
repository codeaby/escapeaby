import React from "react";
import { Box, styled } from "@material-ui/core";
import UnlockPad from "../UnlockPad/UnlockPad";

const StyledContainer = styled(Box)({
  margin: "8px 0",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
});

const ItemButton = styled(Box)({
  width: 54,
  height: 54,
  margin: "0 8px 8px 0",
});

const Interactives = ({
  wall,
  discover,
  unlockedInteractives,
  unlockInteractive,
  items,
}) => {
  return (
    <StyledContainer>
      {wall.interactives.map((interactive) => {
        const isUnlocked = !!unlockedInteractives.find(
          (unlocked) => unlocked === interactive
        );

        const hasItem = items.find(
          (item) => item.id === interactive.itemNeeded
        );

        return (
          <>
            <img
              key={interactive.id}
              src={
                isUnlocked ? interactive.unlockedImg : interactive.initialImg
              }
              width="50%"
            />
            {!isUnlocked && interactive.unlockCode && (
              <UnlockPad
                key={interactive.id}
                unlockCode={interactive.unlockCode}
                unlock={() => {
                  discover(interactive.unlocks);
                  unlockInteractive(interactive);
                }}
              />
            )}
            {!isUnlocked && interactive.itemNeeded && hasItem && (
              <ItemButton
                onClick={() => {
                  discover(interactive.unlocks);
                  unlockInteractive(interactive);
                }}
              >
                <img src={hasItem.img} width="100%"></img>
              </ItemButton>
            )}
          </>
        );
      })}
    </StyledContainer>
  );
};

export default Interactives;
