import { Paper, styled, Typography } from "@material-ui/core";
import React from "react";
import { timerText } from "../../../utils/timeFormatter";

const StyledContainer = styled(Paper)({
  padding: "8px 16px",
  display: "flex",
  justifyContent: "space-between",
});

const GameTitle = ({ name, timer, startingTime }) => {
  return (
    <StyledContainer>
      <Typography variant="h6">{name}</Typography>
      <Typography variant="h6">{timerText(timer, startingTime)}</Typography>
    </StyledContainer>
  );
};

export default GameTitle;
