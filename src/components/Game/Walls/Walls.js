import { Box, IconButton, styled } from "@material-ui/core";
import { ChevronLeft, ChevronRight } from "@material-ui/icons";
import React, { useState } from "react";
import Interactives from "../Interactives/Interactives";

const StyledContainer = styled(Box)({});

const WallsContainer = styled(Box)({
  display: "flex",
  flexDirection: "row",
  margin: "8px 0",
});

const Walls = ({
  room,
  discover,
  unlockedInteractives,
  unlockInteractive,
  items,
}) => {
  const [selectedWall, setSelectedWall] = useState(0);
  discover(room.walls[0].unlocks);

  const navigateWall = (delta) => {
    let newSelectedWall;
    if (selectedWall + delta < 0) {
      newSelectedWall = room.walls.length - 1;
    } else if (selectedWall + delta > room.walls.length - 1) {
      newSelectedWall = 0;
    } else {
      newSelectedWall = selectedWall + delta;
    }
    setSelectedWall(newSelectedWall);
    discover(room.walls[newSelectedWall].unlocks);
  };

  return (
    <StyledContainer>
      <WallsContainer>
        {room.walls.length > 1 && (
          <IconButton onClick={() => navigateWall(-1)}>
            <ChevronLeft />
          </IconButton>
        )}
        <Box style={{ flex: 1 }}>
          <img src={room.walls[selectedWall].img} width="100%" />
        </Box>
        {room.walls.length > 1 && (
          <IconButton onClick={() => navigateWall(+1)}>
            <ChevronRight />
          </IconButton>
        )}
      </WallsContainer>
      <Interactives
        wall={room.walls[selectedWall]}
        discover={discover}
        unlockedInteractives={unlockedInteractives}
        unlockInteractive={unlockInteractive}
        items={items}
      />
    </StyledContainer>
  );
};

export default Walls;
