import { Box, Button, Paper, styled, Typography } from "@material-ui/core";
import React, { useState } from "react";
import Walls from "../Walls/Walls";

const StyledContainer = styled(Paper)({
  margin: "16px 0",
  padding: "8px 0",
});

const RoomButtons = styled(Box)({
  margin: "8px 16px 0",
  display: "flex",
  overflowX: "scroll",
  paddingBottom: "8px",
});

const RoomButton = styled(Button)({
  margin: "0 8px 8px 0",
  flexShrink: "0",
});

const Rooms = ({
  rooms,
  discover,
  unlockedInteractives,
  unlockInteractive,
  items,
}) => {
  const [selectedRoom, setSelectedRoom] = useState(rooms[0]);

  return (
    <StyledContainer>
      <Typography variant="h6" style={{ padding: "0 16px" }}>
        Habitaciones
      </Typography>
      <RoomButtons>
        {rooms.map((room) => (
          <RoomButton
            key={room.id}
            variant={room.id === selectedRoom.id ? "contained" : "outlined"}
            color={room.id === selectedRoom.id ? "primary" : "default"}
            onClick={() => setSelectedRoom(room)}
          >
            {room.name}
          </RoomButton>
        ))}
      </RoomButtons>
      <Typography
        style={{ padding: "8px 16px" }}
        variant="body1"
        align="center"
      >
        {selectedRoom.description}
      </Typography>
      <Walls
        room={selectedRoom}
        discover={discover}
        unlockedInteractives={unlockedInteractives}
        unlockInteractive={unlockInteractive}
        items={items}
      />
    </StyledContainer>
  );
};

export default Rooms;
