import { Box, Dialog, DialogTitle, styled } from "@material-ui/core";
import React from "react";

const StyledImg = styled(Box)({
  padding: "0 16px",
});

const ItemDialog = ({ item, close }) => {
  if (!item) return null;

  return (
    <Dialog onClose={close} open={!!item}>
      <DialogTitle>{item.name}</DialogTitle>
      <StyledImg>
        <img src={item.img} width="100%" />
      </StyledImg>
    </Dialog>
  );
};

export default ItemDialog;
