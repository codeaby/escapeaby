import React, { useState } from "react";
import { Box, Paper, styled, Typography } from "@material-ui/core";
import ItemDialog from "./ItemDialog";

const StyledContainer = styled(Paper)({
  margin: "16px 0",
  padding: "8px 0",
});

const ItemsContainer = styled(Box)({
  display: "flex",
  flexWrap: "wrap",
  margin: "8px 16px",
});

const ItemButton = styled(Box)({
  width: 54,
  height: 54,
  margin: "0 8px 8px 0",
});

const Items = ({ items }) => {
  const [selectedItem, setSelectedItem] = useState(null);

  return (
    <StyledContainer>
      <Typography variant="h6" style={{ padding: "0 16px" }}>
        Items
      </Typography>
      <ItemsContainer>
        {!items.length && <Typography variant="body1">No hay items</Typography>}
        {items.map((item) => (
          <ItemButton key={item.id} onClick={() => setSelectedItem(item)}>
            <img src={item.img} width="100%"></img>
          </ItemButton>
        ))}
      </ItemsContainer>
      <ItemDialog item={selectedItem} close={() => setSelectedItem(null)} />
    </StyledContainer>
  );
};

export default Items;
