import { Box, Paper, styled, Typography } from "@material-ui/core";
import React, { useState } from "react";
import ClueDialog from "./ClueDialog";

const StyledContainer = styled(Paper)({
  margin: "16px 0",
  padding: "8px 0",
});

const CluesContainer = styled(Box)({
  display: "flex",
  flexWrap: "wrap",
  margin: "8px 16px",
});

const ClueButton = styled(Box)({
  width: 54,
  height: 54,
  margin: "0 8px 8px 0",
});

const Clues = ({ clues }) => {
  const [selectedClue, setSelectedClue] = useState(null);

  return (
    <StyledContainer>
      <Typography variant="h6" style={{ padding: "0 16px" }}>
        Pistas
      </Typography>
      <CluesContainer>
        {!clues.length && (
          <Typography variant="body1">No hay pistas</Typography>
        )}
        {clues.map((clue) => (
          <ClueButton key={clue.id} onClick={() => setSelectedClue(clue)}>
            <img src={clue.img} width="100%"></img>
          </ClueButton>
        ))}
      </CluesContainer>
      <ClueDialog clue={selectedClue} close={() => setSelectedClue(null)} />
    </StyledContainer>
  );
};

export default Clues;
