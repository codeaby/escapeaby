import {
  Box,
  Dialog,
  DialogTitle,
  styled,
  Typography,
} from "@material-ui/core";
import React from "react";

const StyledImg = styled(Box)({
  padding: "0 16px",
});

const ClueDescription = styled(Typography)({
  padding: "8px 16px",
  textAlign: "center",
});

const ClueDialog = ({ clue, close }) => {
  if (!clue) return null;

  return (
    <Dialog onClose={close} open={!!clue}>
      <DialogTitle>{clue.name}</DialogTitle>
      <StyledImg>
        <img src={clue.img} width="100%" />
      </StyledImg>
      <ClueDescription variant="body1">{clue.text}</ClueDescription>
    </Dialog>
  );
};

export default ClueDialog;
