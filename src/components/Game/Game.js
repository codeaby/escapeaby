import { Container, Dialog, Typography } from "@material-ui/core";
import React, { useCallback, useEffect, useState } from "react";
import useInterval from "../../hooks/useInterval";
import { getRoom, getClue, getItem } from "../../services/EscapeabyService";
import { timerText } from "../../utils/timeFormatter";
import Loading from "../Loading/Loading";
import Clues from "./Clues/Clues";
import GameTitle from "./GameTitle/GameTitle";
import Items from "./Items/Items";
import Rooms from "./Rooms/Rooms";

const Game = ({ selectedGame, backToHome }) => {
  const [loading, setLoading] = useState(true);
  const [startingTime] = useState(new Date());
  const [timer, setTimer] = useState(new Date());
  const [rooms, setRooms] = useState([]);
  const [clues, setClues] = useState([]);
  const [items, setItems] = useState([]);
  const [unlockedInteractives, setUnlockedInteractives] = useState([]);
  const [win, setWin] = useState(false);
  const [winingTime, setWinningTime] = useState(null);

  useInterval(() => setTimer(Date.now()), 1000);

  const unlockInteractive = (interactive) => {
    setUnlockedInteractives((prev) => [...prev, interactive]);
  };

  const getUnlock = async (unlock, alreadyUnlocked, apiCall, setUnlocked) => {
    const found = alreadyUnlocked.find((unlocked) => unlocked.id === unlock.id);
    if (!found) {
      const { data } = await apiCall(unlock.id);
      setUnlocked((prev) => [...prev, data]);
    }
  };

  const discover = useCallback(
    (unlocks) => {
      unlocks.forEach(async (unlock) => {
        if (unlock.type === "CLUE") {
          getUnlock(unlock, clues, getClue, setClues);
        } else if (unlock.type === "ITEM") {
          getUnlock(unlock, items, getItem, setItems);
        } else if (unlock.type === "EXIT") {
          setWin(true);
          setWinningTime(new Date());
        }
      });
    },
    [clues, items]
  );

  const loadStartingRoom = useCallback(async () => {
    const found = rooms.find((room) => room.id === selectedGame.startingRoom);
    if (!found) {
      const { data: room } = await getRoom(selectedGame.startingRoom);
      setRooms((prev) => [...prev, room]);
      discover(room.unlocks);
    }
    setLoading(false);
  }, [rooms, selectedGame, discover]);

  useEffect(loadStartingRoom, [loadStartingRoom]);

  return (
    <Container style={{ marginTop: 16 }}>
      <GameTitle
        name={selectedGame.name}
        timer={timer}
        startingTime={startingTime}
      />
      <Loading loading={loading} />
      {!loading && (
        <>
          <Rooms
            rooms={rooms}
            discover={discover}
            unlockedInteractives={unlockedInteractives}
            unlockInteractive={unlockInteractive}
            items={items}
          />
          <Clues clues={clues} />
          <Items items={items} />
        </>
      )}
      <Dialog open={win} onClose={backToHome}>
        <Typography variant="h5" align="center" style={{ padding: 50 }}>
          Has Ganado en {timerText(winingTime, startingTime)}!!!
        </Typography>
      </Dialog>
    </Container>
  );
};

export default Game;
