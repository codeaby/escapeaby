import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Container,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { getGames } from "../../services/EscapeabyService";
import Loading from "../Loading/Loading";

const GamesList = ({ selectGame }) => {
  const [games, setGames] = useState([]);
  const [loading, setLoading] = useState(true);

  const loadGamesList = async () => {
    const { data } = await getGames();
    setGames(data.games);
    setLoading(false);
  };

  useEffect(() => {
    loadGamesList();
  }, []);

  return (
    <Container style={{ marginTop: 16 }}>
      <Loading loading={loading} />
      {games.map((game) => (
        <Card key={game.id}>
          <CardMedia
            style={{ height: 0, paddingTop: "56.25%" }}
            image={game.img}
            title={game.name}
          />
          <CardHeader
            title={game.name}
            subheader={game.description}
          ></CardHeader>
          <CardContent style={{ padding: "0 16px" }}>
            <Typography
              variant="caption"
              color="textSecondary"
              component="p"
              align="left"
            >
              por {game.author}
            </Typography>
          </CardContent>
          <CardActions style={{ justifyContent: "flex-end" }}>
            <Button
              size="small"
              color="default"
              onClick={() => selectGame(game)}
            >
              Jugar
            </Button>
          </CardActions>
        </Card>
      ))}
    </Container>
  );
};

export default GamesList;
