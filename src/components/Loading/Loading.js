import { Box, CircularProgress } from "@material-ui/core";
import React from "react";

const Loading = ({ loading }) => {
  if (!loading) return null;

  return (
    <Box style={{ display: "flex", margin: 32, justifyContent: "center" }}>
      <CircularProgress />
    </Box>
  );
};

export default Loading;
