import {
  AppBar,
  createMuiTheme,
  CssBaseline,
  IconButton,
  ThemeProvider,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Home } from "@material-ui/icons";
import { useState } from "react";
import Game from "./components/Game/Game";
import GamesList from "./components/GamesList/GamesList";

function App() {
  const theme = createMuiTheme({
    palette: {
      type: "dark",
    },
  });

  const [selectedGame, setSelectedGame] = useState(null);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" style={{ flexGrow: 1 }}>
            Escapeaby
          </Typography>
          <IconButton
            edge="end"
            color="inherit"
            aria-label="menu"
            onClick={() => {
              setSelectedGame(null);
            }}
          >
            <Home />
          </IconButton>
        </Toolbar>
      </AppBar>
      {!selectedGame && <GamesList selectGame={setSelectedGame} />}
      {selectedGame && (
        <Game
          selectedGame={selectedGame}
          backToHome={() => {
            setSelectedGame(null);
          }}
        />
      )}
    </ThemeProvider>
  );
}

export default App;
