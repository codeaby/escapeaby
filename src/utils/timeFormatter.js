import { differenceInSeconds } from "date-fns";

export const padWithZero = (value) => value.toString().padStart(2, "0");

export const timerText = (timer, startingTime) => {
  const diff = differenceInSeconds(timer, startingTime);

  return `${padWithZero(Math.floor(diff / 60))}:${padWithZero(diff % 60)}`;
};
