import axios from "axios";

// const BASE_URL = "http://localhost:5001/escapeaby/us-central1";
const BASE_URL = "https://us-central1-codeaby-486b9.cloudfunctions.net";

export const getGames = async () => axios.get(`${BASE_URL}/escapeaby/games`);

export const getRoom = async (id) =>
  axios.get(`${BASE_URL}/escapeaby/rooms/${id}`);

export const getClue = async (id) =>
  axios.get(`${BASE_URL}/escapeaby/clues/${id}`);

export const getItem = async (id) =>
  axios.get(`${BASE_URL}/escapeaby/items/${id}`);
